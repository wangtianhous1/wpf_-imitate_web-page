using GalaSoft.MvvmLight;
using System.Collections.ObjectModel;

namespace WpfApp1_Test.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
            ///
            InitPerson();
        }

        /// <summary>
        /// Person����
        /// </summary>
        private Person person;
        public Person Personal
        {
            get { return person; }
            set
            {
                if (person != value)
                    person = value;
                RaisePropertyChanged("Personal");
            }
        }
        public ObservableCollection<Person> Persons { get; set; }

    
        private void InitPerson()
        {
            Persons = new ObservableCollection<Person>();
            Person person = new Person { Name = "Tom", Age = 25 };
            Persons.Add(person);
            person = new Person { Name = "Jack", Age = 23 };
            Persons.Add(person);
            person = new Person { Name = "Lucy", Age = 32 };
            Persons.Add(person);
            person = new Person { Name = "HanMeimei", Age = 18 };
            Persons.Add(person);
            person = new Person { Name = "Lilei", Age = 18 };
            Persons.Add(person);
            person = new Person { Name = "ZhangThree", Age = 13 };
            Persons.Add(person);
            person = new Person { Name = "LeeFour", Age = 24 };
            Persons.Add(person);
            person = new Person { Name = "WangFive", Age = 35 };
            Persons.Add(person);
        }
    }
}