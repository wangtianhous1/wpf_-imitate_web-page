﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace WpfApp_ImitationPage//.ConverClass
{
    [ValueConversion(typeof(decimal), typeof(Brush))]
    public class Salary2ForeColor : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            decimal salaryNu = (decimal)value;
            return salaryNu > 300 ? Brushes.Red : salaryNu < 200 ? Brushes.Green : Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
