﻿using System;

namespace WpfApp_ImitationPage.ViewModel
{
    internal class DateValue
    {
        public DateValue()
        {
        }

        public string Date { get; set; }
        public int Value { get; set; }
    }
}