﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_ImitationPage.ViewModel
{
    public class EmployeesInfo
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public decimal SalaryRmb { get; set; }
        public string Contry { get; set; }

    }
}
