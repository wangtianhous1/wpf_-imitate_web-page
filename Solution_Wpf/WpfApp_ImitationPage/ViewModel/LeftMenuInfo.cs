﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_ImitationPage.ViewModel
{
    public class LeftMenuInfo 
    {
        public string Name { get; set; }
        public string ChineseName { get; set; }
        public string IcoPath { get; set; }

        public string ImagePngPath { get; set; }

    }
}
