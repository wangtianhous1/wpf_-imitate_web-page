using GalaSoft.MvvmLight;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using System;
using System.Collections.ObjectModel;

namespace WpfApp_ImitationPage.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            InitLeftMenuInfos();
            InitRightFirstInfos();
            OxySingleTestModel = new SingleOxyModelText();
            InitOxyMutelShowInfos();
            InitTasksBlockInfos();
            InitEmployeesInfos();
        }

        Random random = new Random();

        private void InitEmployeesInfos()
        {
            this.EmployeesInfos = new ObservableCollection<EmployeesInfo>();

            string strTemp = "sfdjsla;jgsaDHfa2sd5fsfaasIFGs";

            for (int i = 0; i < 10; i++)
            {
                EmployeesInfos.Add(new EmployeesInfo
                {
                    ID = i,
                    SalaryRmb = random.Next(100) * 102.2m,
                    Name= strTemp.Substring(random.Next(strTemp.Length-2),2),
                    Contry = strTemp.Substring(random.Next(strTemp.Length - 4), 4).ToUpper(),
                });
            }

        }

        private void InitTasksBlockInfos()
        {
            TasksBlockInfos = new ObservableCollection<TasksBlockInfo>();

            TasksBlockInfos.Add(new TasksBlockInfo
            {
                IsCheck = true,
                OptionShowInfo = "Night after night, she came to tuck me in?",
            });

            TasksBlockInfos.Add(new TasksBlockInfo
            {
                IsCheck = false,
                OptionShowInfo = "she came to tuck me in?",
            });
        }

        private void InitOxyMutelShowInfos()
        {
            OxyMutelShowInfos = new ObservableCollection<OxyShowInfo>();

            var firstOxy = new OxyShowInfo()
            {
                CurBackGroundColorHex = "#489A4E",
                CurPlotModel = new OxyPlot.PlotModel()
                {

                },
                BottomTitle = "Daily Sales",
                BottomShowMgs = "55% increase in today sales",
                UpDataMgs = "updated 4 minutes ago",
            };
            //添加一条曲线
            LineSeries line1 = new LineSeries() { MarkerType = MarkerType.Circle, LineStyle = LineStyle.Automatic, InterpolationAlgorithm = InterpolationAlgorithms.CanonicalSpline, Color = OxyColor.Parse("#F8F2D1"), FontSize = 50 };
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                line1.Points.Add(new DataPoint(i * 10, random.Next(50)));
            }
            firstOxy.CurPlotModel.Series.Add(line1);
            OxyMutelShowInfos.Add(firstOxy);


            var secondOxy = new OxyShowInfo()
            {
                CurBackGroundColorHex = "#4C4EBF",
                CurPlotModel = new OxyPlot.PlotModel()
                {

                },
                BottomTitle = "Email Subscriptions",
                BottomShowMgs = "Last Campagn Performance",
                UpDataMgs = "campagn sent 2 days ago",
            };
            //添加一条曲线
            LineSeries line2 = new LineSeries() { MarkerType = MarkerType.Circle, LineStyle = LineStyle.Automatic, Color = OxyColor.Parse("#F8F2D1"), FontSize = 50 };

            for (int i = 0; i < 10; i++)
            {
                line2.Points.Add(new DataPoint(i * 10, random.Next(50)));
            }
            secondOxy.CurPlotModel.Series.Add(line2);
            OxyMutelShowInfos.Add(secondOxy);


            var thirddOxy = new OxyShowInfo()
            {
                CurBackGroundColorHex = "#F45036",
                CurPlotModel = new OxyPlot.PlotModel()
                {

                },
                BottomTitle = "Completed Tasks",
                BottomShowMgs = "Last Campagn Performance",
                UpDataMgs = "campagn sent 2 days ago",
            };
            //添加一条曲线
            LineSeries line3 = new LineSeries() { MarkerType = MarkerType.Circle, LineStyle = LineStyle.Automatic, InterpolationAlgorithm = InterpolationAlgorithms.CanonicalSpline, Color = OxyColor.Parse("#F8F2D1"), FontSize = 50 };

            for (int i = 0; i < 10; i++)
            {
                line3.Points.Add(new DataPoint(i * 10, random.Next(50)));
            }
            thirddOxy.CurPlotModel.Series.Add(line3);
            OxyMutelShowInfos.Add(thirddOxy);

        }

        //添加右侧第一个模块信息
        private void InitRightFirstInfos()
        {
            RightShowFirstInfos = new ObservableCollection<RightShowFirstInfo>();

            RightShowFirstInfos.Add(new RightShowFirstInfo
            {
                TopColorHex = "#FD9C0E",
                TopShowIocPath = "/Images/copy.png",
                ShowInfoMgs = "Used Spance",
                ShowNumberMgs = "49/50",
                UintStr = "GB",
                ShowHintMgsIoc = "/Images/alarm.png",
                ShowHintMgs = "Get More Space.",
            });

            RightShowFirstInfos.Add(new RightShowFirstInfo
            {
                TopColorHex = "#3F48CC",
                TopShowIocPath = "/Images/house.png",
                ShowInfoMgs = "Revenue",
                ShowNumberMgs = "$34,254",
                UintStr = "",
                ShowHintMgsIoc = "/Images/dataPackage.png",
                ShowHintMgs = "Last 24 Hours",
            });

            RightShowFirstInfos.Add(new RightShowFirstInfo
            {
                TopColorHex = "#F75843",
                TopShowIocPath = "/Images/waring.png",
                ShowInfoMgs = "Fixed Issues",
                ShowNumberMgs = "75",
                UintStr = "",
                ShowHintMgsIoc = "/Images/tag.png",
                ShowHintMgs = "Tracked trom Github",
            });

            RightShowFirstInfos.Add(new RightShowFirstInfo
            {
                TopColorHex = "#04A7CA",
                TopShowIocPath = "/Images/twitter.png",
                ShowInfoMgs = "Followers",
                ShowNumberMgs = "+245",
                UintStr = "",
                ShowHintMgsIoc = "/Images/resit.png",
                ShowHintMgs = "Tracked",
            });


        }


        //加载左侧菜单栏信息
        private void InitLeftMenuInfos()
        {
            LeftMenuInfos = new ObservableCollection<LeftMenuInfo>();

            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "Dashboard",
                ChineseName = "仪表盘",
                IcoPath = "&#xe600;",
                ImagePngPath = "LeftMenuImages/微软.png",
            }); ;

            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "User Profile",
                ChineseName = "用户文件",
                IcoPath = "&#xe8c8;",
                ImagePngPath = "LeftMenuImages/224用户.png",
            });

            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "Table List",
                ChineseName = "表格列表",
                IcoPath = "&#xe607;",
                ImagePngPath = "LeftMenuImages/记事本.png",
            });

            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "Typography",
                ChineseName = "排印",
                IcoPath = "&#xe601;",
                ImagePngPath = "LeftMenuImages/复制.png",
            });

            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "Icons",
                ChineseName = "图标",
                IcoPath = "&#xe61d;",
                ImagePngPath = "LeftMenuImages/icon_色彩.png",
            });

            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "Maps",
                ChineseName = "地图",
                IcoPath = "&#xec3f;",
                ImagePngPath = "LeftMenuImages/地图-地标.png",
            });


            LeftMenuInfos.Add(new LeftMenuInfo
            {
                Name = "Notifications",
                ChineseName = "通知",
                IcoPath = "&#xe8c0;",
                ImagePngPath = "LeftMenuImages/211铃铛.png",
            });
        }

        public ObservableCollection<LeftMenuInfo> LeftMenuInfos { get; set; }

        public ObservableCollection<RightShowFirstInfo> RightShowFirstInfos { get; set; }

        public SingleOxyModelText OxySingleTestModel { get; set; }

        public ObservableCollection<OxyShowInfo> OxyMutelShowInfos { get; set; }

        public ObservableCollection<TasksBlockInfo> TasksBlockInfos { get; set; }

        public ObservableCollection<EmployeesInfo> EmployeesInfos { get; set; }


    }
}