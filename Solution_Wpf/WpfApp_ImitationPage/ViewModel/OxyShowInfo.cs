﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_ImitationPage.ViewModel
{
    public class OxyShowInfo
    {
        public PlotModel CurPlotModel { get; set; }

        public string CurBackGroundColorHex { get; set; }

        public string BottomTitle { get; set; }
        public string BottomShowMgs { get; set; }
        public string UpDataMgs { get; set; }
    }
}
