﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_ImitationPage.ViewModel
{
    public class RightShowFirstInfo
    {
        public string TopColorHex { get; set; }
        public string TopShowIocPath { get; set; }
        public string ShowInfoMgs { get; set; }
        public string ShowNumberMgs { get; set; }
        public string UintStr { get; set; }

        public string ShowHintMgsIoc { get; set; }
        public string ShowHintMgs { get; set; }
    }
}
