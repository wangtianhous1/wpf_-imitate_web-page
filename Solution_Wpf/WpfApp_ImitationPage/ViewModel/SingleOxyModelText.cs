﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp_ImitationPage.ViewModel
{
    public class SingleOxyModelText
    {
        public PlotModel TestPlotModel { get; set; }

        public string BackgroundColorHex { get; set; }


        public SingleOxyModelText()
        {
            TestPlotModel = new PlotModel() ;
            //TestPlotModel.Title = "Test";


            LineSeries lineOne = new LineSeries() {  MarkerType = MarkerType.Circle,Color=OxyColor.FromRgb(255,255,255) };
            //lineOne.InterpolationAlgorithm = InterpolationAlgorithms.CanonicalSpline;
            lineOne.FontSize = 30;

            Random random = new Random();
            for (int i = 0; i < 7; i++)
            {
                lineOne.Points.Add(new DataPoint(i, random.Next(41)));
            }

            TestPlotModel.Series.Add(lineOne);

            BackgroundColorHex = "#489A4E";
        }

    }
}
